#! /usr/bin/env pyhton

from setuptools import find_packages, setup

with open("README.md", "r") as des:
    l_desc = des.read()

setup(
    name='gnpp',
    packages=find_packages(),
    version='0.1.0',
    package_data={'': ['assets/*']},
    entry_points={'console_scripts': ['gnpp = gnpp.cli:cli']},
    description='Simple tool for generate new project python',
    url='https://gitlab.com/nesstero/gnpp',
    license='MIT',
    author='nestero',
    author_email='nestero@mail.com',
    long_description=l_desc,
    long_description_content_type='text/markdown',
)
